﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MVCSeleniumTest
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTestMovie
    {
        public UnitTestMovie()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestCreateMethod()
        {
            //
            // TODO: Add test logic here
            //
            IWebDriver driver = new ChromeDriver();

            driver.Navigate().GoToUrl("http://localhost:65393/");
            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl("http://localhost:65393/Home/Create");

            IWebElement searchInput = driver.FindElement(By.Id("name"));
            searchInput.SendKeys("Terminator");
            searchInput = driver.FindElement(By.Id("genre"));
            searchInput.SendKeys("Ciencia Ficción");
            searchInput = driver.FindElement(By.Id("premiereYear"));
            searchInput.SendKeys("2000");
            searchInput.Submit();
           
        }
    }
}
