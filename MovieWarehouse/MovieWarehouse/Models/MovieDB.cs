﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace MovieWarehouse.Models
{
    public class MovieDB : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
    }
}