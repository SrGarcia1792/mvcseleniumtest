﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieWarehouse.Models
{
    public class Movie
    {
        public int Id { get; set;}
        public string name { get; set; }
        public string genre { get; set; }
        public int premiereYear { get; set; }

    }
}