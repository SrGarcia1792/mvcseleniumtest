﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace MovieWarehouse.Automation
{
    public class BrowserAutomation
    {
        public void RunBrowser(string host){
            

            IWebDriver driver = new ChromeDriver();
           
            driver.Navigate().GoToUrl(host);
            driver.Manage().Window.Maximize();

        }
    }
}